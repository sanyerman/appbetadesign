package com.agilebusiness.betaapp.ui.general;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import com.agilebusiness.betaapp.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

 class Fragment_General extends Fragment {

    BarChart barChartGeneral;
    BarChart BarChartGeneralGroup;
    private String[] months = new String[]{"Oct","Sep","Aug","Jul","Jun","May","Apr","Mar","Feb","Jan"};
    public float[] sale =  new float[]{8.2f,8.3f,8.2f,8.1f,8.0f,7.9f,7.9f,7.9f,7.8f,7.2f};
    //private int[] colors =  new int[]{Color.DKGRAY,Color.RED,Color.BLUE,Color.YELLOW,Color.GREEN,Color.CYAN,Color.GRAY,Color.LTGRAY,R.color.purple_500,Color.MAGENTA};
    private int[] colors =  new int[]{Color.RED,Color.GREEN,Color.GREEN,Color.GREEN,Color.GREEN,Color.GREEN,Color.GREEN,Color.GREEN,Color.GREEN,Color.GREEN};


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_general, container, false);
        barChartGeneral = (BarChart) root.findViewById(R.id.generalGraph);
        BarChartGeneralGroup = (BarChart) root.findViewById(R.id.generalGraphGroup);
        createCharts();
        createChartsGroup(BarChartGeneralGroup);
        return root;
    }

    private void createChartsGroup(BarChart mpBarChart) {
        BarDataSet barDataSet = new BarDataSet(barEntries1(),"Pres");
        barDataSet.setColor(Color.BLUE);
        barDataSet.setValueTextSize(15);
        BarDataSet barDataSet2 = new BarDataSet(barEntries2(),"2021");
        barDataSet2.setColor(Color.GREEN);
        barDataSet2.setValueTextSize(15);
        BarDataSet barDataSet3 = new BarDataSet(barEntries3(),"2020");
        barDataSet3.setColor(Color.RED);
        barDataSet3.setValueTextSize(15);


        BarData barData = new BarData(barDataSet,barDataSet2,barDataSet3);

        mpBarChart.setData(barData);



        String[] days = new String[]{"Ene","Feb","Mar","Abr","May","Jun","Jul"};
        XAxis xAxis =mpBarChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);
        xAxis.setTextSize(25f);


        float barSpace = 0.05f;
        float groupSpace = 0.16f;

        barData.setBarWidth(0.25f);

        mpBarChart.getXAxis().setAxisMinimum(0);
        //mpBarChart.getXAxis().setAxisMaximum(0+mpBarChart.getBarData().getGroupWidth(groupSpace,barSpace * 7));
        mpBarChart.setVisibleXRangeMaximum(2);
        mpBarChart.getAxisLeft().setAxisMinimum(0);
        mpBarChart.groupBars(0,groupSpace,barSpace);
        mpBarChart.getAxisLeft().setDrawGridLines(false); mpBarChart.getXAxis().setDrawGridLines(false);
        mpBarChart.animateY(2000);
        mpBarChart.setDrawBarShadow(false);
        mpBarChart.invalidate();
    }

    private Chart getSameChart(Chart chart, String description, int textColor, int background, int animateY){
        chart.getDescription().setText(description);
        chart.getDescription().setTextSize(15);
        chart.setBackgroundColor(background);
        chart.animateY(animateY);
        legend(chart);
        return chart;
    }

    private void legend(Chart chart){
        Legend legend = chart.getLegend();
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        legend.setTextSize(15);

        ArrayList<LegendEntry> entries = new ArrayList<>();
        for (int i=0; i<months.length; i++){
            LegendEntry entry = new LegendEntry();
            entry.formColor = colors[i];
            entry.label = months[i];
            entries.add(entry);
        }
        legend.setCustom(entries);
    }



    private ArrayList<BarEntry> getBarEntries(){

        ArrayList<BarEntry> entries = new ArrayList<>();
        for (int i=0; i<sale.length; i++){
            entries.add(new BarEntry(i,sale[i]));
        }




        return entries;
    }

    private void axisX(XAxis axis){
        axis.setGranularityEnabled(true);
        axis.setPosition(XAxis.XAxisPosition.BOTTOM); /* Position text title by Bar*/
        axis.setValueFormatter(new IndexAxisValueFormatter(months));
        axis.setAxisMaximum(10);
        axis.setEnabled(false);
    }

    private void axisLeft(YAxis axis){
        axis.setAxisMaximum(10);
        axis.setAxisMinimum(-10);
        axis.setGranularity(0.5f);
        axis.setEnabled(true);
    }

    private void axisRight(YAxis axis){
        axis.setEnabled(false);
    }

    public void createCharts(){
        barChartGeneral = (BarChart) getSameChart(barChartGeneral, "",Color.WHITE,Color.WHITE,2000);
        barChartGeneral.setDrawGridBackground(false); /* view line BarChart*/
        barChartGeneral.setDrawBarShadow(false); /* rellena la barrra hasta su 100% */
        barChartGeneral.setData(getBarData()); /* call grafica de barras*/
        barChartGeneral.setVisibleXRangeMaximum(5);
        barChartGeneral.invalidate();


        axisX(barChartGeneral.getXAxis());

        //axisLeft(barChartGeneral.getAxisLeft());
        axisRight(barChartGeneral.getAxisRight());


    }

    private DataSet getData(DataSet dataSet){
        dataSet.setColors(colors);
        dataSet.setValueTextColor(Color.BLACK);
        dataSet.setValueTextSize(15);
        return dataSet;
    }

    private BarData getBarData(){
        BarDataSet barDataSet = (BarDataSet) getData(new BarDataSet(getBarEntries(),""));
        barDataSet.setBarShadowColor(Color.GRAY);
        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(0.80f);
        return barData;
    }
    private ArrayList<BarEntry> barEntries1(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,8.5f));
        barEntries.add(new BarEntry(2,8.6f));
        barEntries.add(new BarEntry(3,8.7f));
        barEntries.add(new BarEntry(4,8.5f));
        barEntries.add(new BarEntry(5,8.4f));
        barEntries.add(new BarEntry(6,8.4f));
        barEntries.add(new BarEntry(7,8.6f));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries2(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,8.1f));
        barEntries.add(new BarEntry(2,7.9f));
        barEntries.add(new BarEntry(3,7.9f));
        barEntries.add(new BarEntry(4,7.8f));
        barEntries.add(new BarEntry(5,8.3f));
        barEntries.add(new BarEntry(6,8.2f));
        barEntries.add(new BarEntry(7,7.9f));


        return barEntries;
    }

    private ArrayList<BarEntry> barEntries3(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,7.3f));
        barEntries.add(new BarEntry(2,7.4f));
        barEntries.add(new BarEntry(3,7.5f));
        barEntries.add(new BarEntry(4,7.8f));
        barEntries.add(new BarEntry(5,8.2f));
        barEntries.add(new BarEntry(6,7.9f));
        barEntries.add(new BarEntry(7,7.8f));


        return barEntries;
    }



}