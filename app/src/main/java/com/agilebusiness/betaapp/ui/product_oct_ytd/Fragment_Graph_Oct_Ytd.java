package com.agilebusiness.betaapp.ui.product_oct_ytd;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.agilebusiness.betaapp.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;


public class Fragment_Graph_Oct_Ytd extends Fragment {
    private BarChart mpBarChart;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_gallery, container, false);

        mpBarChart = (BarChart) root.findViewById(R.id.BarChart2);

        BarDataSet barDataSet = new BarDataSet(barEntries1(),"Dic");
        barDataSet.setColor(Color.BLUE);
        BarDataSet barDataSet2 = new BarDataSet(barEntries2(),"Nov");
        barDataSet2.setColor(Color.GREEN);
        BarDataSet barDataSet3 = new BarDataSet(barEntries3(),"Oct");
        barDataSet3.setColor(Color.RED);
        BarDataSet barDataSet4 = new BarDataSet(barEntries4(),"Sep");
        barDataSet4.setColor(Color.YELLOW);

        BarData barData = new BarData(barDataSet,barDataSet2,barDataSet3,barDataSet4);

        mpBarChart.setData(barData);
        mpBarChart.setVisibleXRangeMaximum(5);


        String[] days = new String[]{"Ene","Feb","Mar","Abr","May","Jun","Jul"};
        XAxis xAxis =mpBarChart.getXAxis();
        xAxis.setValueFormatter(new IndexAxisValueFormatter(days));
        xAxis.setCenterAxisLabels(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularity(1);
        xAxis.setGranularityEnabled(true);

        mpBarChart.setDragEnabled(true);


        float barSpace = 0.05f;
        float groupSpace = 0.16f;

        barData.setBarWidth(0.25f);

        mpBarChart.getXAxis().setAxisMinimum(0);
        //mpBarChart.getXAxis().setAxisMaximum(0+mpBarChart.getBarData().getGroupWidth(groupSpace,barSpace * 7));

        mpBarChart.getAxisLeft().setAxisMinimum(0);
        mpBarChart.groupBars(0,groupSpace,barSpace);
        mpBarChart.invalidate();

        return root;

    }

    private ArrayList<BarEntry> barEntries1(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,2000));
        barEntries.add(new BarEntry(2,234));
        barEntries.add(new BarEntry(3,240));
        barEntries.add(new BarEntry(4,643));
        barEntries.add(new BarEntry(5,982));
        barEntries.add(new BarEntry(6,1023));
        barEntries.add(new BarEntry(7,284));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries2(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,2323));
        barEntries.add(new BarEntry(2,563));
        barEntries.add(new BarEntry(3,1234));
        barEntries.add(new BarEntry(4,1902));
        barEntries.add(new BarEntry(5,324));
        barEntries.add(new BarEntry(6,123));
        barEntries.add(new BarEntry(7,1567));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries3(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,637));
        barEntries.add(new BarEntry(2,982));
        barEntries.add(new BarEntry(3,1238));
        barEntries.add(new BarEntry(4,1236));
        barEntries.add(new BarEntry(5,1989));
        barEntries.add(new BarEntry(6,235));
        barEntries.add(new BarEntry(7,1222));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries4(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,765));
        barEntries.add(new BarEntry(2,1789));
        barEntries.add(new BarEntry(3,1800));
        barEntries.add(new BarEntry(4,300));
        barEntries.add(new BarEntry(5,1999));
        barEntries.add(new BarEntry(6,2000));
        barEntries.add(new BarEntry(7,1445));



        return barEntries;
    }



}