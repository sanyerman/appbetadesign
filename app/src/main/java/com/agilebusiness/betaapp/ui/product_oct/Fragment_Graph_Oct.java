package com.agilebusiness.betaapp.ui.product_oct;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.agilebusiness.betaapp.R;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarEntry;

import java.util.ArrayList;

public class Fragment_Graph_Oct extends Fragment {
    BarChart mpBarChart;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_slideshow, container, false);
        mpBarChart = (BarChart) root.findViewById(R.id.OctGraph);
        return root;
    }

    private ArrayList<BarEntry> barEntries1(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,2000));
        barEntries.add(new BarEntry(2,791));
        barEntries.add(new BarEntry(3,630));
        barEntries.add(new BarEntry(4,458));
        barEntries.add(new BarEntry(5,2724));
        barEntries.add(new BarEntry(6,500));
        barEntries.add(new BarEntry(7,2173));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries2(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,900));
        barEntries.add(new BarEntry(2,631));
        barEntries.add(new BarEntry(3,1040));
        barEntries.add(new BarEntry(4,382));
        barEntries.add(new BarEntry(5,2614));
        barEntries.add(new BarEntry(6,5000));
        barEntries.add(new BarEntry(7,1173));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries3(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,400));
        barEntries.add(new BarEntry(2,231));
        barEntries.add(new BarEntry(3,100));
        barEntries.add(new BarEntry(4,768));
        barEntries.add(new BarEntry(5,1514));
        barEntries.add(new BarEntry(6,9874));
        barEntries.add(new BarEntry(7,1273));

        return barEntries;
    }

    private ArrayList<BarEntry> barEntries4(){
        ArrayList<BarEntry> barEntries = new ArrayList<>();
        barEntries.add(new BarEntry(1,100));
        barEntries.add(new BarEntry(2,291));
        barEntries.add(new BarEntry(3,1230));
        barEntries.add(new BarEntry(4,1168));
        barEntries.add(new BarEntry(5,114));
        barEntries.add(new BarEntry(6,950));
        barEntries.add(new BarEntry(7,173));

        return barEntries;
    }
}